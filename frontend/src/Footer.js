import React, {Component} from 'react';
import './App.css';
import './Footer.css';

class Footer extends Component{
    render(){
        return(
            <div className="footer">
                <div className="foot1">
                    <p>Listening to music</p>
                    <p>has never been so easy</p>
                    <p>Home</p>
                    <p>About</p>
                    <p>Privacy</p>
                </div>
                <div className="foot2">
                    <p>&#169; 2018, Remaster. All Rights Reserved.</p>
                </div>
            </div>
        );
    }
}

export default Footer;