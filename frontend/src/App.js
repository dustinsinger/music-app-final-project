import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Room from './Room.js'
import Home from './Home.js'
import './App.css';
import io from 'socket.io-client';

class App extends Component {
  constructor() {
    super();
    this.socket = io('http://178.128.224.76:4000', {
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      reconnectionAttempts: 99999
    });
    this.createRoom = this.createRoom.bind(this);
    this.joinRoom = this.joinRoom.bind(this)
    this.handleChange = this.handleChange.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.state = {
      roomName: '',
      password: '',
      roomCreated: false,
      identity: '',
      nowPlaying: ''
    }
  }

  componentDidMount() {
    this.socket.on('joinRoom', (res) => {
      if (res.success === true) {
        this.setState({ roomCreated: true, identity: 'client', nowPlaying: res.nowPlaying, roomName: res.roomName });
      }
      else {
        console.log('room does not exist');
      }
    })

    this.socket.on('createRoom', (response) => {
      if (response.success === false) {
        console.log(response.message);
      }
      else {
        this.setState({ roomName: response.roomName, roomCreated: true, identity: 'host' })
      }
    })
  }

  // returnRoom(routerData){

  //   <Room identity={this.state.identity} socket={this.socket} roomName={this.state.roomName} nowPlaying={this.state.nowPlaying}></Room>

  // }

  handlePassword(e) {
    this.setState({ password: e.target.value })
  }

  handleChange(e) {
    this.setState({ roomName: e.target.value });
  }

  createRoom(e, roomName, password) {
    e.preventDefault();
    this.socket.emit('createRoom', { roomName: roomName, password: password });
  }

  joinRoom(e, roomName, password) {
    e.preventDefault();
    this.socket.emit('joinRoom', { roomName: roomName, password: password });
  }

  render() {
    if (this.state.roomCreated === false) {
      // return (
      //   <div className="App">
      //     <input type="text" onChange={this.handleChange} />
      //     <input type='password' onChange={this.handlePassword} />
      //     <button onClick={this.createRoom}>Create Room</button>
      //     <button onClick={this.joinRoom}>Join Room</button>
      //   </div>
      // );
      return (
        <Home joinRoom={this.joinRoom} createRoom={this.createRoom}></Home>
      );
    }
    else {
      return (
        <Room identity={this.state.identity} socket={this.socket} roomName={this.state.roomName} nowPlaying={this.state.nowPlaying}></Room>
      );
    }
  }
}

export default App;
