import React, { Component } from 'react';
import anime from 'animejs';
import './Home.css';


let animateCreateButton = () => {
    anime({
        targets: document.getElementById('createButton'),
        scale: 1.1,
        duration: 1000,
        easing: 'easeOutCirc',
        translateY: '-10vh'
    })
}


let animateJoinButton = () => {
    anime({
        targets: document.getElementById('joinButton'),
        scale: 1.1,
        duration: 1000,
        easing: 'easeOutCirc',
        translateY: '-10vh'
    })
}

let reverseCreateButtonAnimation = () => {
    anime({
        targets: document.getElementById('createButton'),
        scale: 1,
        duration: 1000,
        easing: 'easeOutCirc',
        translateY: '-10vh'
    })
}


let reverseJoinButtonAnimation = () => {
    anime({
        targets: document.getElementById('joinButton'),
        scale: 1,
        duration: 1000,
        easing: 'easeOutCirc',
        translateY: '-10vh'
    })
}

let animateTitleCharacters = () => {
    anime({
        targets: document.getElementById('letter4'),
        opacity: 1,
        duration: 1250,
        delay: 150,
        easing: 'linear'
    })

    anime({
        targets: document.getElementById('letter1'),
        opacity: 1,
        duration: 1250,
        delay: 600,
        easing: 'linear'
    })

    anime({
        targets: document.getElementById('letter5'),
        opacity: 1,
        duration: 1250,
        delay: 1050,
        easing: 'linear'
    })

    anime({
        targets: document.getElementById('letter3'),
        opacity: 1,
        duration: 1250,
        delay: 1500,
        easing: 'linear'
    })

    anime({
        targets: document.getElementById('letter2'),
        opacity: 1,
        duration: 1250,
        delay: 1950,
        easing: 'linear'
    })
}


let animateButtonsPositions = () => {
    anime({
        targets: document.getElementById('createButton'),
        duration: 1000,
        delay: 2300,
        easing: 'easeOutCirc',
        translateY: '-10vh',
        opacity: 1

    })

    anime({
        targets: document.getElementById('joinButton'),
        duration: 1000,
        delay: 2300,
        easing: 'easeOutCirc',
        translateY: '-10vh',
        opacity: 1

    })
}


let animateHP = () => {
    anime({
        targets: document.getElementById('homepageContainer'),
        backgroundColor: '#040426',
        duration: 750,
        easing: 'linear',
        delay: 0
    })

    anime({
        targets: document.getElementById('letter1'),
        color: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('letter3'),
        color: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('letter4'),
        color: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('letter5'),
        color: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('homepageSlogan'),
        color: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('createButton'),
        color: '#ffffff',
        borderColor: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })

    anime({
        targets: document.getElementById('joinButton'),
        color: '#ffffff',
        borderColor: '#ffffff',
        duration: 1000,
        easing: 'easeOutCirc',
        delay: 0
    })
}

let animateCRTransition = () => {
    animateHP();
    anime({
        targets: document.getElementById('homepageContainer'),
        translateX: '-100vw',
        duration: 500,
        easing: 'easeOutCirc',
        delay: 1100
    })
}

let animateJRTransition = () => {
    animateHP();
    anime({
        targets: document.getElementById('homepageContainer'),
        translateX: '100vw',
        duration: 500,
        easing: 'easeOutCirc',
        delay: 1100
    })
}

let animateCRValidationButton = () =>{
    anime({
        targets: document.getElementById('crValidation'),
        scale: 1.1,
        duration: 1000,
        easing: 'easeOutCirc'
    })
}

let animateJRValidationButton = () =>{
    anime({
        targets: document.getElementById('jrValidation'),
        scale: 1.1,
        duration: 1000,
        easing: 'easeOutCirc'
    })
}


let reverseCRValidationAnimation = () =>{
    anime({
        targets: document.getElementById('crValidation'),
        scale: 1,
        duration: 1000,
        easing: 'easeOutCirc'
    })
}

let reverseJRValidationAnimation = () =>{
    anime({
        targets: document.getElementById('jrValidation'),
        scale: 1,
        duration: 1000,
        easing: 'easeOutCirc'
    })
}

class Home extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.joinRoom = this.joinRoom.bind(this);
        this.state = {
            password: '',
            roomName: '',
            createRoom: true
        }
    }

    componentDidMount() {
        {
            document.addEventListener("DOMContentLoaded", function () {
                animateTitleCharacters();
                animateButtonsPositions();

                document.getElementById('createButton').addEventListener("mouseenter", function () {
                    animateCreateButton();
                })

                document.getElementById('joinButton').addEventListener("mouseenter", function () {
                    animateJoinButton();
                })

                document.getElementById('createButton').addEventListener("mouseleave", function () {
                    reverseCreateButtonAnimation();
                })

                document.getElementById('joinButton').addEventListener("mouseleave", function () {
                    reverseJoinButtonAnimation();
                })

                document.getElementById('createButton').addEventListener("click", function () {
                    animateCRTransition();
                })

                document.getElementById('joinButton').addEventListener("click", function () {
                    animateJRTransition();
                })






                document.getElementById('createButton').addEventListener("touchstart", function () {
                    animateCreateButton();
                })

                document.getElementById('joinButton').addEventListener("touchstart", function () {
                    animateJoinButton();
                })

                document.getElementById('createButton').addEventListener("touchend", function () {
                    reverseCreateButtonAnimation();
                })

                document.getElementById('joinButton').addEventListener("touchend", function () {
                    reverseJoinButtonAnimation();
                })










                document.getElementById("crValidation").addEventListener("mouseenter", function(){
                    animateCRValidationButton();
                })

                document.getElementById("crValidation").addEventListener("mouseleave", function(){
                    reverseCRValidationAnimation();
                })

            })


        }
    }

    handlePassword(e) {
        this.setState({ password: e.target.value })
    }

    handleChange(e) {
        this.setState({ roomName: e.target.value });
    }

    joinRoom(e) {
        e.preventDefault();
        this.setState({ createRoom: false });
    }

    render() {
        if (this.state.createRoom === true) {
            return (
                <div id="homepage">
                    <div id="homepageContainer">
                        <div id="homepageTitle">
                            <div id="letter1">m</div>
                            <div id="letter2">o</div>
                            <div id="letter3">t</div>
                            <div id="letter4">i</div>
                            <div id="letter5">f</div>
                        </div>
                        <div id="homepageSlogan">- The Spirit of Group Music -</div>
                        <button id="createButton" onClick={this.createRoom}>Create Room</button>
                        <button id="joinButton" onClick={this.joinRoom}>Join Room</button>

                    </div>
                    <div id="createRoomContainer">
                        <form>
                            <p id="crTitle">Create Room</p>
                            <input type="text" id="crUsername" onChange={this.handleChange} placeholder="Room Name" />
                            <input type='password' id="crPassword" onChange={this.handlePassword} placeholder="Password" />
                            <button id="crValidation" onClick={(e) => this.props.createRoom(e, this.state.roomName, this.state.password)}>Create Room</button>
                        </form>

                    </div>
                </div>
            );
        }
        else {
            return (
                <div id="homepage">
                    <div id="homepageContainer">
                        <div id="homepageTitle">
                            <div id="letter1">m</div>
                            <div id="letter2">o</div>
                            <div id="letter3">t</div>
                            <div id="letter4">i</div>
                            <div id="letter5">f</div>
                        </div>
                        <div id="homepageSlogan">- The Spirit of Group Music -</div>
                        <button id="createButton" onClick={this.createRoom}>Create Room</button>
                        <button id="joinButton" onClick={this.joinRoom}>Join Room</button>

                    </div>
                    <div id="joinRoomContainer">
                        <form>
                            <p id="jrTitle">Join Room</p>
                            <input type="text" id="jrUsername" onChange={this.handleChange} placeholder="Room Name" />
                            <input type='password' id="jrPassword" onChange={this.handlePassword} placeholder="Password" />
                            <button id="jrValidation" onClick={(e) => this.props.joinRoom(e, this.state.roomName, this.state.password)}>Join Room</button>
                        </form>

                                        {
                        document.addEventListener("DOMContentLoaded", function(){
                            document.getElementById("jrValidation").addEventListener("mouseenter", function(){
                                animateJRValidationButton();
                                console.log("hello");
                            })
                        })
                    }
                    </div>
                </div>
            );
        }
    }
}

export default Home;